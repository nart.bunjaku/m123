[TOC]

# DHCP

### DHCP IP-Adressvergabe bei den Clients aktivieren
Um die IP-Adressen via DHCP zu beziehen, wählte ich diese aus und füllte das "DHCP zur Konfiguration verwenden" Feld aus.
<img src="img/client1.png" width="1775px">
<img src="img/client2.png" width="1775px">

### DHCP-Server konfigurieren
Danach klickte ich den DHCP-Server an und wählte unten rechts den Button "DHCP-Server einrichten" aus. Danach kam folgendes Pop-up:
<img src="img/dhcp.png" width="850px"><br>
Ich überprüfte die Adress-Untergrenze und Adress-Obergrenze, bei welchem ich feststellte, dass die IP-Range bereits korrekt ist.<br>
Dann wählte ich den zweiten Reiter und mir wurde folgendes präsentiert:
<img src="img/static.png" width="850px"><br>
Ich gab die MAC-Adresse und gewünschte IP-Adresse des Client 3 ein. Dann wählte ich "Hinzufügen"

### Aktionsmodus
Dann wollte ich überprüfen, ob alles funktioniert. Ich schaute den Datenaustausch des Servers an.
<img src="img/datenaustausch1.png" width="1775px"><br>
In dem Bild ist die IP-vergabe mit DHCP zu sehen. Da ich nicht verstanden habe, was bei den grünen Einträgen passiert, schaute ich unten nach:
<img src="img/datenaustausch2.png" width="850px"><br>
Ich verstand dadurch, dass der Client hier überprüft, ob die IP-Adressen bereits eine MAC-Adresse zugewiesen haben, bevor sie diese IP-Adresse requesten.

### IP-Adresse von Client 3 überprüfen
Ebenfalls wollte ich überprüfen, ob der Client 3 die IP-Adreses besitzt, welche ich diesem im DHCP-Server zugewiesen habe.
<img src="img/client3.png" width="1775px"><br>
Da die IP-Adresse stimmte, war ich zufrieden damit.

# DHCP mit Cisco Packet Tracer
## Routerkonfiguration auslesen
- Für welches Subnetz ist der DHCP Server aktiv?
Nach dem eingeben des Commands show running-configuration ist dies zu sehen. Unter ip dhcp pool 1 nach network ist die IP des Pools und die Subnet des Pools zu sehen. Die Subnet des Pools ist 255.255.255.0. <br> <img src="./img/command3.png">
- Welche IP-Adressen ist vergeben und an welche MAC-Adressen?
Nach dem eingeben des Commands show ip dhcp binding ist dies zu sehen. Unter IP adress sind die Adressen zu sehen und unter Hardware adress deren korrespondierende MAC-Adressen. <br> <img src="./img/command2.png">

| IP-Adresse | MAC-Adresse |
| ---- | ----- |
| 192.168.25.28 | 0009.7CA6.4CE3 |
| 192.168.25.29 | 00D0.BC52.B29B |

- In welchem Range vergibt der DHCP-Server IPv4 Adressen?
Unter IP address range ist diese sichtbar. sie ist 192.168.25.28 - 192.168.25.187 und 192.168.25.239 - 192.168.25.254
- Was hat die Konfiguration ip dhcp excluded-address zur Folge?
Damit wird definiert, welche IP-Adressen nicht vergeben werden. <br> <img src="./img/command3.png"> <br> Dabei ist die erste IP angabe der start und die zweite das ende des ausgeschlossenen bereiches.
- Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?
Insgesammt können 254 vergeben werden.

## DORA - DHCP Lease beobachten
- Welcher OP-Code hat der DHCP-Offer?<br>
Er hat den code OP:0x0000000000000002
<br> <img src="./img/offer.png">

- Welcher OP-Code hat der DHCP-Request?<br>
Er hat den code OP:0x0000000000000001
<br> <img src="./img/request.png"> 

- Welcher OP-Code hat der DHCP-Acknowledge?<br>
Er hat den code OP:0x0000000000000002
<br> <img src="./img/acknowledge.png">

- An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?<br>
Es wird an die IP-Adresse 255.255.255.255 geschickt. Dies ist unter DST IP unter dem Abschnitt IP sichtbar
<br> <img src="./img/discover.png">

- An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?<br>
Es wird and die MAC-Adresse FFFF.FFFF.FFFF geschickt. Dies ist unter EthernetII unter DEST ADDR sichtbar. Sie ist broadcast als MAC-Adresse

- Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?
Da der DORA-Prozess über broadcast verläuft.

- Gibt es ein DHCP-Relay in diesem Netzwerk? Wenn ja, welches Gerät ist es? Wenn nein, wie kann das mithilfe der DHCP-PDUs festgestellt werden?
Sie ist 0.0.0.0, welches die Absenderadresse eines Gerätes ohne IP-Adresse im Netzwerk ist.

- Welche IPv4-Adresse wird dem Client zugewiesen?
In meinem Fall ist es die IP 192.168.25.28.
## Netzwerk umkonfigurieren
<br> <img src="./img/serverip.png">
<br> <img src="./img/servergateway.png">
<br> <img src="./img/website.png">

## DHCP Auftrag mit Ubuntu Server
Bei diesem Auftrag mussten wir eine VM mit Ubuntu aufsetzen und dort dann den DHCP-Server Konfigurieren. Ich habe es mit Ioannis gemacht. 
"Ich habe gemeinsam mit einem Ihm die VMware Workstation geöffnet und eine neue VM erstellt. Wir haben sie gemäß den Anforderungen konfiguriert und anschließend gestartet. Während des Starts mussten wir noch einige Kleinigkeiten konfigurieren. Bisher lief alles reibungslos. Nach Abschluss der Konfiguration erschien jedoch kontinuierlich die Fehlermeldung: "[Failed] Failed unmounting /cdrom. Bitte entfernen Sie das Installationsmedium und drücken Sie dann ENTER." Nach dieser Meldung konnten wir nicht weiterkommen. Zuerst haben wir versucht, die VM neu zu starten, aber leider trat die gleiche Fehlermeldung erneut auf. Also haben wir eine neue VM erstellt und versucht, alles noch einmal zu konfigurieren. Dieses Mal kamen wir ein Stück weiter, jedoch stießen wir auch hier auf ein Problem. Es erschien ein Text mit der Aufforderung "ubuntu-server login:"."
