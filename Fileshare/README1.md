Arbeitsauftrag "FileShare auf Linux einrichten"

Aufgabenstellung: Mein zugewiesener Task bestand darin, zwei VMs bereitzustellen. Eine VM musste ich mit Linux aufsetzen (Ubuntu-Desktop-Version), während die andere mit Windows betrieben wurde. Die Linux-VM war bereits aus vorherigen Aufträgen vorhanden. Wir hatten die Aufgabe sicherzustellen, dass beide VMs bis Layer 3 verbunden waren und sich gegenseitig pingen konnten. Anschließend mussten wir den SMB-Dienst auf der Linux-VM installieren. Danach starteten wir die Windows-VM und überprüften, ob Verzeichnisse gemountet und genutzt werden konnten (Erstellung von Ordnern und Dateien). Während der Lösung der Aufgabe dokumentierten wir regelmäßig den Fortschritt und erstellten Screenshots.

Schritt 1: Ubuntu-Desktop-Version herunterladen (bereits vorhanden). 

Schritt 2: Einrichten der beiden VMs (keine Bilder, da dies bereits oft durchgeführt wurde).

Schritt 3: Ausführen des Befehls "System Update", um sicherzustellen, dass alle installierten Pakete auf dem neuesten Stand sind. 

Schritt 4: Eingabe des Befehls "systemctl status smbd --no-pager -l" zur Überprüfung, ob der Dienst aktiv und ausgeführt wird. Das Ergebnis sollte folgendermaßen aussehen: 
![Bild8](Images/MicrosoftTeams-image__2_.png)

Schritt 5: Eingabe des Befehls "sudo systemctl enable --now smbd" zur Aktivierung des Dienstes, sodass er beim Systemstart automatisch gestartet wird.
Weitere Befehle: "sudo ufw allow samba" - ermöglicht den externen Zugriff auf den Dienst von außerhalb des Computers.
"sudo usermod -aG sambashare $USER" - fügt den aktuellen Benutzer zur SambaShare-Gruppe hinzu, um Zugriff auf freigegebene Dateien und Ordner zu ermöglichen.
"sudo smbpasswd -a $USER" 

Schritt 6: Konfiguration des Benutzers für die Verwendung von Samba. Angenommen, man möchte den Ordner "Bilder" oder einen anderen freigeben, der dem aktuellen Benutzer gehört. ![Bild7](Images/MicrosoftTeams-image__6_.png)

Schritt 7: Auswahl des freizugebenden Ordners, Rechtsklick darauf, Auswahl von Eigenschaften und dann der Registerkarte "Local Network Share". Aktivierung der Optionen "Allow others to create and delete files in this folder" und "Guest access", falls gewünscht. Klick auf "Create", um den Freigabepunkt zu erstellen. ![Bild6](Images/MicrosoftTeams-image__9_.png)

Schritt 8: Klicken auf "Add the permissions automatically". 
![Bild5](Images/MicrosoftTeams-image__3_.png)

Schritt 9: Öffnen des Dateimanagers, Klicken auf "Other locations", und Eingabe von "smb://ip-address/shared-folder-name" im "Connect to Server"-Feld. Eingabe des Ubuntu-Benutzernamens und Passworts.![Bild4](Images/MicrosoftTeams-image__8_.png)

Wahrscheinlich musste man hier den Ubuntu Anmeldesachen eingeben
![Bild3](Images/MicrosoftTeams-image__5_.png)

Schritt 10: Wechsel zur Windows-VM, Öffnen des Explorers, Auswahl von "This PC", Rechtsklick an leerer Stelle, Auswahl von "Add a network location".
![Bild1](Images/MicrosoftTeams-image__10_.png)

Schritt 11: Eingabe der IP-Adresse und des Ordner-Namens, z. B. \server-ip-address\shared-folder. Ersetzen der server-ip-address durch die IP-Adresse des Ubuntu-Servers mit der SAMBA-Konfiguration und dem Namen des freigegebenen Ordners. Eingabe von Benutzername und Passwort bei Aufforderung.
![Bild2](Images/MicrosoftTeams-image__4_.png)

Normalerweise sollte der freigegebene Ordner von Ubuntu über das SMB-Protokoll auf der Windows-VM remote oder lokal gemountet sein. Hier stieß ich jedoch auf einen Fehler.


Troubleshooting-Beschreibung:

Der Großteil der Einrichtung verlief problemlos, jedoch stieß ich auf Schwierigkeiten im letzten Schritt. Die VMs konnten sich erfolgreich pingen, aber die freigegebenen Ordner von Ubuntu tauchten nicht wie erwartet auf. Trotz intensiver Untersuchung des Problems, sowohl allein als auch mit meinen Kollegen, konnte keine Lösung gefunden werden.

Selbst nach erneutem Starten beider VMs und dem Versuch, den gesamten Vorgang von Grund auf neu zu wiederholen, blieb das Problem bestehen. Interessanterweise hatte auch mein Kollege Ioannis mit dem gleichen Problem zu kämpfen. Gemeinsam haben wir versucht, das Problem zu lösen, aber auch die Unterstützung unseres Chefs brachte keine Lösung.

Leider blieb der Grund für das Fehlverhalten unklar, und wir waren nicht in der Lage, die freigegebenen Ordner erfolgreich zwischen den VMs zu verbinden. Falls jemand ähnliche Herausforderungen hatte oder weitere Ideen zur Fehlerbehebung hat, wäre ich sehr dankbar für dessen Unterstützung.
