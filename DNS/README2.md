**Aufgabenstellung:**

Meine Verantwortlichkeit bestand darin, eine virtuelle Maschine (VM) unter Verwendung des Betriebssystems **Windows** einzurichten und daraufhin die **DNS-Konfiguration** durchzuführen. Dies war ein entscheidender Schritt, um sicherzustellen, dass die VM ordnungsgemäß im Netzwerk kommunizieren konnte.

**Teamarbeit:**

In diesem Projekt arbeitete ich eng mit meinen Kollegen **Valentino**, **Anik** und **Ioanis** zusammen. Unsere Aufgaben waren deutlich aufgeteilt, und wir begannen mit der Umsetzung.

2. **Start der Server Manager-Anwendung:** Die Server Manager-Anwendung wurde auf unserem Windows-Server geöffnet. ![](/Images/1.png)

3. **Hinzufügen der DNS-Rolle:** Im Server Manager wählten wir die Option "Rollen und Features hinzufügen" und wählten dann den DNS-Server als zu installierende Rolle aus.![](/Images/13.png)

4. **Installation des DNS:** Die DNS-Rolle wurde ausgewählt, und der Installationsprozess wurde durchgeführt.![](/Images/5.png)

5. **Konfiguration des DNS-Dienstes:** Nach der Installation konfigurierten wir den DNS-Dienst über den Server Manager, um sicherzustellen, dass er ordnungsgemäß funktionierte.

6. **Überprüfung mit nslookup:** Abschließend verwendeten wir **nslookup**, um sicherzustellen, dass die DNS-Konfiguration funktionierte und Domain-Namen korrekt aufgelöst werden konnten.

1. **Erstellung der Domäne:** Wir erstellten die Domäne mit einem eindeutigen Namen.![](/Images/7.png)

2. **Hinzufügen von Benutzern und Computern:** Nach der Domänen erstellung fügten wir das Benutzerkonto und den Client hinzu und integrierten sie in die Domäne.

**Fehlerbehebungsbeschreibung:**

Während des Projekts stießen wir auf ein Problem beim DNS-Setup. Obwohl das Pingen des Clients erfolgreich war, funktionierte **nslookup** nicht wie erwartet. Offensichtlich gab es ein Problem mit der **Domänenkonfiguration**.

Leider konnten wir das Problem mit der DNS-Konfiguration am Ende nicht lösen. Selbst unser Lehrer, Herr Calisto, hatte keine Lösung auf Lager. Manchmal sind technische Probleme wirklich knifflig und nicht einfach zu beheben. Dennoch war es eine lehrreiche Erfahrung, an diesem Problem zu arbeiten, da wir gelernt haben, dass nicht alles sofort reibungslos verläuft. Wir sollten dies als Gelegenheit nutzen, unsere Fähigkeiten weiter zu verbessern.